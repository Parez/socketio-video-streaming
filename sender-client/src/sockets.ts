import * as socketIo from 'socket.io-client';

const SERVER_URL = 'http://localhost:3000';

export class SocketService {
  private socket;

  public initSocket(): void {
    this.socket = socketIo(SERVER_URL);
    this.socket.on('connect', () => {
      console.log("Connected");
    });
    this.socket.connect();
  }

  public sendVideoData(message: any): void {
    this.socket.emit('video_data', message);
    // console.log("TEST");
    // this.socket.emit('video_data', 'Test');
  }

}
