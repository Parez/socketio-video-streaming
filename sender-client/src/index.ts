import MediaStreamRecorder = require('msr');
import {SocketService} from './sockets';

const socketService: SocketService = new SocketService();

socketService.initSocket();

export async function startRecord(): Promise<MediaStream> {
  return new Promise((resolve, reject) => {
    navigator.getUserMedia({audio: false, video: true}, (stream: MediaStream) => {
      resolve(stream);
    }, (error) => {
      reject(error);
    });
  });
}

document.addEventListener("DOMContentLoaded", initApp, false);

function initApp(): void {
  console.log("Ready");
  document.getElementById("startBtn").addEventListener("click", () => {
    console.log("Works");
    startRecord().then((stream: MediaStream) => {
      const videoElem: HTMLVideoElement = (document.getElementById("preview") as HTMLVideoElement);
      videoElem.srcObject = stream;
      console.log(videoElem, stream);
      console.log("Success");

      var mediaRecorder = new MediaStreamRecorder(stream);
      mediaRecorder.mimeType = 'video/webm; codecs="vp8"';
      mediaRecorder.ondataavailable = function (blob) {
        console.log("Chunk Ready");
        socketService.sendVideoData({timestamp: Date.now(), data: blob});
      };
      mediaRecorder.start(300);

      /*const recorderOptions = {
        audioBitsPerSecond : 128000,
        videoBitsPerSecond : 2500000,
        mimeType : 'video/mp4'
      };

      var mediaRecorder = new MediaRecorder(stream);
      mediaRecorder.onstart = function(e) {
        this.chunks = [];
      };
      mediaRecorder.ondataavailable = function(e) {
        this.chunks.push(e.data);
      };
      mediaRecorder.onstop = function(e) {
        var blob = new Blob(this.chunks, { 'type' : 'video/mp4' });
        socket.emit('radio', blob);
      };

      // Start recording
      mediaRecorder.start();

      // Stop recording after 5 seconds and broadcast it to server
      setTimeout(function() {
        mediaRecorder.stop()
      }, 5000);*/

    }, err => {
      console.log("Error", err);
    });
  });
}

