import * as socketIo from 'socket.io-client';

const SERVER_URL = 'http://localhost:3000';

export class SocketService {
  private socket;
  private ms: MediaSource = new MediaSource();
  private sourceBuffer: SourceBuffer;
  private onVideoUpdate: () => void;

  private isVideoReady: boolean = false;
  private onAppendFunc: EventListener;
  private sentTimestamp: number;
  constructor() {
    this.ms.addEventListener ('sourceopen', (e) => {
      if (this.isVideoReady) return;
      console.log("OPENED");
      this.sourceBuffer = this.ms.addSourceBuffer('video/webm; codecs="vp8"');
      this.sourceBuffer.mode = 'sequence';
      this.isVideoReady = true;
    }, false);


  }

  public initSocket(): void {
    this.socket = socketIo(SERVER_URL);
    this.socket.on('connect', () => {
      console.log("Connected");
    });
    this.socket.connect();

    this.socket.on('update_video', ({data, timestamp}) => {
      if (this.onAppendFunc) {
        this.sourceBuffer.removeEventListener('updateend', this.onAppendFunc);
      }
      console.log("Video");
      if (this.isVideoReady) {
        console.log("APPEND");
        this.sourceBuffer.appendBuffer(new Uint8Array(data));

        this.onAppendFunc = () => {
          this.onBufferAppended(timestamp);
        };

        this.sourceBuffer.addEventListener('updateend', this.onAppendFunc);

      }
    });
  }

  private onBufferAppended(timestamp: number): void {
    console.log("Update End");
    console.log("Buffered", this.sourceBuffer.timestampOffset);
    this.onVideoUpdate();
    console.log(Date.now() - timestamp);
  }

  public listenToVideo(callback?: (data?: any) => void): void {
    this.onVideoUpdate = callback;
  }

  public get video(): MediaSource {
    return this.ms;
  }

}
