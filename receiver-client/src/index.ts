import {SocketService} from './sockets';

const socketService: SocketService = new SocketService();

document.addEventListener("DOMContentLoaded", initApp, false);

function initApp(): void {
  console.log("Ready");

  document.getElementById("receiveBtn").addEventListener("click", startReceiving);
}

function startReceiving(): void {
  socketService.initSocket();
  const videoElem: HTMLVideoElement = (document.getElementById("preview") as HTMLVideoElement);
  videoElem.src = URL.createObjectURL(socketService.video);
  socketService.listenToVideo(() => {
    console.log("Received");
    try {
      videoElem.play();
    } catch  (e) {
      console.log(e);
    }
  });
}

