const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);

let clientId = 0;

io.on('connection', function(socket){
  console.log('Client connected', clientId);
  socket.on('video_data', function (data) {
    console.log('Data Received', data);
    io.sockets.emit ('update_video', data);
  });

  socket.on('disconnect', () => {
    console.log('Client disconnected', clientId);
  });
  clientId ++;
});

server.listen(3000, function(){
  console.log('listening on *:3000');
});
